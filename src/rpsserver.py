import yaml, usertoken, game
from flask import Flask, jsonify, request
app = Flask(__name__)


DEFAULT_TIMEOUT = None
HOST = None
PORT = None




with open("config/config.yaml", 'r') as config:
    options = yaml.load(config, yaml.FullLoader)
    DEFAULT_TIMEOUT = int(options["default_timeout"])
    HOST = options["host"]
    PORT = int(options["port"])

# code that manages server functions of our game

class RPSServer:

    def __init__(self):


    def waitForPlayer(self, userToken, timeOut=DEFAULT_TIMEOUT):
        # todo
        return -1

    @app.route("/token", methods=['GET'])
    def get_token(self):
        return jsonify(usertoken.UserToken())

    @app.route("/game", methods=['GET'])
    def get_game(self):
        return jsonify(game.Game(request.get_json()["UserToken"]), self)

    @app.route("/game", method=['POST'])
    def post_game(self):
        query = request.get_json()
        if len(query) == 2:
            gameToJoin = query["GameCode"]
            userJoining = query["UserToken"]

        if len(query) == 3:
            gamePlaying = query["GameCode"]
            userPlaying = query["UserToken"]
            choice = query["Choice"]


