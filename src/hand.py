from enum import Enum

# represents a selection in a player's hand
class Selection(Enum):
    ROCK = 1
    PAPER = 2
    SCISSORS = 3
    ERROR = 4


# represents the hand a player is going to play as well as methods related to that
class Hand:
    def __init__(self, selectedHand):
        self.__selectedHand = selectedHand

    # evaluates which hand wins
    # return values:
    # 1 if this hand wins
    # -1 if the other hand wins
    # 0 in the event of a tie
    # -2 in the event of an error
    def compare(self, otherHand):
        if self.__selectedHand == otherHand.__selectedHand:
            return 0

        elif self.__selectedHand == Selection.ROCK:
            if otherHand.__selectedHand == Selection.PAPER:
                return -1
            elif otherHand.__selectedHand == Selection.SCISSORS:
                return 1
            else:
                return -2

        elif self.__selectedHand == Selection.PAPER:
            if otherHand.__selectedHand == Selection.ROCK:
                return 1
            elif otherHand.__selectedHand == Selection.SCISSORS:
                return -1
            else:
                return -2

        elif self.__selectedHand == Selection.SCISSORS:
            if otherHand.__selectedHand == Selection.ROCK:
                return -1
            elif otherHand.__selectedHand == Selection.PAPER:
                return 1
            else:
                return -2

        else:
            return -2