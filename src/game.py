import gamecode, usertoken, threading, rpsserver, hand

# class that handles game logic

class Game(threading.Thread):
    __games = {}

    def getGameByCode(gameCode):
        for eachGame in Game.__games:
            if eachGame.__gameCode == gameCode:
                return eachGame

        return None


    def __init__(self, creatorToken, server):
        self.__gameCode = gamecode.Gamecode()
        self.__creatorToken = creatorToken
        self.__playerToken = None
        self.__server = server
        Game.__games[self.__gameCode] = self

    def addUser(self, playerToken):
        if self.__playerToken != None:
            raise Exception

        self.__playerToken = playerToken


    def run(self):
        while(self.__playerToken == None):
            continue

        play1 = self.__server.waitForPlay(self.__creatorToken)
        play2 = self.__server.waitForPlay(self.__playerToken)

        if play1 == -1 or play2 == -1:
            raise Exception

        result = play1.compare(play2)
        Game.__games[self.__gameCode] = None

        if result == -2:
            raise Exception
        elif result == -1:
            return self.__playerToken
        elif result == 0:
            return None
        else:
            return self.__creatorToken
