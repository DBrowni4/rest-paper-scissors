import random, yaml, string

TOKEN_LENGTH = 0

with open("config/config.yaml", 'r') as config:
    TOKEN_LENGTH = int(yaml.load(config, yaml.FullLoader)["token_length"])

# represents a token, or a user identifier
class UserToken:
    __activeTokens = []

    def __init__(self):
        self.__value = None
        self.__registerToken()

    def __registerToken(self):
        attemptedToken = ""
        validChars = string.ascii_lowercase + string.ascii_uppercase + "1234567890"

        while self.__value == None:
            while(len(attemptedToken) < TOKEN_LENGTH):
                attemptedToken += validChars[random.randint(0, len(validChars) - 1)]

            if attemptedToken in UserToken.__activeTokens:
                attemptedToken = ""
                continue

            self.__value = attemptedToken
            UserToken.__activeTokens.append(self.__value)