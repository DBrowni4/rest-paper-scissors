import random, yaml, string

# represents a game code to identify a game
GAMECODE_LENGTH = 0

with open("../config/config.yaml", "r") as config:
    GAMECODE_LENGTH = int(yaml.load(config, yaml.FullLoader)["gamecode_length"])

class Gamecode:
    __activeGames = []

    def __init__(self):
        self.__value = None
        self.registerGameCode()

    def registerGameCode(self):
        attemptedCode = ""
        validChars = string.ascii_uppercase

        while(self.__value == None):
            while(len(attemptedCode) < GAMECODE_LENGTH):
                attemptedCode += validChars[random.randint(0, len(validChars) - 1)]

            if attemptedCode in Gamecode.__activeGames:
                attemptedCode = ""
                continue

            self.__value = attemptedCode
            Gamecode.__activeGames.append(self.__value)

    def getGameCode(self):
        return self.__value