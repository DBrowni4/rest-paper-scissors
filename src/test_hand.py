import pytest
from hand import *


@pytest.fixture
def test_Hands():
    return {"ROCK": Hand(Selection.ROCK), "PAPER": Hand(Selection.PAPER), "SCISSORS": Hand(Selection.SCISSORS)}


def tester(test_Hands, hand1, hand2, result):
    assert(test_Hands[hand1].compare(test_Hands[hand2]) == result)


def test_runner(test_Hands):
    tester(test_Hands, "ROCK", "PAPER", -1)
    tester(test_Hands, "ROCK", "ROCK", 0)
    tester(test_Hands, "ROCK", "SCISSORS", 1)

    tester(test_Hands, "PAPER", "SCISSORS", -1)
    tester(test_Hands, "PAPER", "PAPER", 0)
    tester(test_Hands, "PAPER", "ROCK", 1)

    tester(test_Hands, "SCISSORS", "ROCK", -1)
    tester(test_Hands, "SCISSORS", "SCISSORS", 0)
    tester(test_Hands, "SCISSORS", "PAPER", 1)